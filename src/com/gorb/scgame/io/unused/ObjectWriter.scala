package com.gorb.scgame.io.unused

import com.gorb.scgame.{School, Spell, Persona, Arcana}

/**
 * Created by Squall on 07/08/2015.
 */
object ObjectWriter {
  val personaFile: String = "persona.txt"

  val arcanaMap: Map[String, Arcana] = Map("FOO" -> Arcana("Fool"), "MAG" -> Arcana("Magician"),
    "PRI" -> Arcana("Priestess"), "EMPS" -> Arcana("Empress"), "EMPR" -> Arcana("Emperor"),
    "HIER" -> Arcana("Hierophant"), "LOVE" -> Arcana("Lovers"), "CHAR" -> Arcana("Chariot"),
    "STR" -> Arcana("Strength"), "JUST" -> Arcana("Justice"), "HERM" -> Arcana("Hermit"),
    "FORT" -> Arcana("Fortune"), "TMPR" -> Arcana("Temperence"), "SUN" -> Arcana("Sun"))

  val schoolMap: Map[String, School] = Map("PHY" -> School("Physical"), "FIR" -> School("Fire"),
    "ICE" -> School("Ice"), "WIN" -> School("Wind"), "THU" -> School("Thunder"), "ALM" -> School("Almighty"))

  val spellMap: Map[String, Spell] = Map("ZIO1" -> Spell("Zio", schoolMap("THU")), "SKEW" -> Spell("Skewer", schoolMap("PHY")),
    "CLEAV" -> Spell("Cleave", schoolMap("PHY")))

  val p1: Persona = Persona("Izanagi", arcanaMap("FOO"), Vector[Spell](spellMap("ZIO1"), spellMap("CLEAV")))
  val p2: Persona = Persona("Yomotsu-Shikome", arcanaMap("FOO"), Vector[Spell](spellMap("SKEW")))

  def getPers = p1
}

object TestObjs extends App {
  println(ObjectWriter.p1)
  println(ObjectWriter.p2)
}
