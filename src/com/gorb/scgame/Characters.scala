package com.gorb.scgame

import java.io._

/**
 * Created by Cloud on 21/07/2015.
 */
trait Characters {

}

case class Arcana(name: String) {
  override def toString: String = name
}

case class Persona(name: String, arcana: Arcana, spellList: Vector[Spell]) {

  def prettySpells: String = {
    var spellString: String = ""
    for (i <- spellList.indices) {
      spellString += "Spell_" + i + " " + spellList(i) + "\n"
    }
    spellString
  }

  override def toString: String = "Persona: " + name + " " + "Arcana: " + arcana.toString + "\n" + "Spells:" + "\n" + prettySpells
}

case class Spell(name: String, school: School) {
  override def toString: String = name + " " + "Type: " + school.toString
}

case class School(name: String) {
  override def toString: String = name
}

object TestObjWrite extends App {
  val priestess: Arcana = new Arcana("Priestess")
  val file: File = new File("arcana.txt")
  try {
    val fos = new FileOutputStream(file)
    val os: ObjectOutputStream = new ObjectOutputStream(fos)
    os.writeObject(priestess)
    os.close()
  }
  catch {
    case ex: Exception => ex.printStackTrace()
  }

}

object TestObjRead extends App {
  val file: File = new File("arcana.txt")
  val inputList: Vector[Arcana] = readArcanaFile(file)


  def readArcanaFile(file: File): Vector[Arcana] = {
    try {
      val fis = new FileInputStream(file)
      val ois = new ObjectInputStream(fis)
      val readIn = ois.readObject()
      readIn match {
        case a1: Arcana => Vector[Arcana](a1)
        case _ => println("Dang"); null
      }
    } catch {
      case ex: Exception => println("Failed Read"); ex.printStackTrace(); null
    }
  }
  println(inputList)
}